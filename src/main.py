import sys
from Report import Report

# Convert `area_titles.csv` into a dictionary
#############################################
#This is the dictionary for the full stats
answer = {}
#This is the dictionary for the software stats
copyArray = {}
l = []
l.append(sys.argv[1])
l.append('/area_titles.csv')
#areaTitles = '\\data\DC\area_titles.csv'
l1 = ''.join(l)
with open(l1) as infile:
    infile.readline()  # we don't care about the header row
    for line in infile:
        #Splits the area fips and 
        gender = line.split(",",1)
        #Removes unnecessary details
        if(gender[0].startswith('"C') or gender[0].startswith('"US') or gender[0].endswith('000"') or gender[0].startswith('"area')):
            pass
        else:
            #Saves the details in the array
            answer[gender[0]] = [gender[1], 0, 0, 0, 0]
            copyArray[gender[0]] = [gender[1], 0, 0, 0, 0]
# Collect stats from `2017.annual.singlefile.csv`
################################################
#Splicer for the actual data
dataFromLine = []
q = []
q.append(sys.argv[1])
q.append('/2017.annual.singlefile.csv')
t = ''.join(q)

fipsTotalNumber = 0
totalMaxAnnualWage = 0
totalMaxAnnualWageName = ""
totalGrossWages = 0
totalAreaMostEstablishmentsName = ""
totalAreaMostEstablishmentsAmount = 0
totalEstablishment = 0
totalAreaHighEmploymentName = ""
totalAreaHighEmployment = 0
totalEmployment = 0


fipsSoftNumber = 0
softwareMaxAnnualWage = 0
softwareMaxAnnualWageName = ""
softGrossWages = 0
softwareAreaMostEstablishmentsName = ""
softwareAreaMostEstablishmentsAmount = 0
softEstablishment = 0
softwareAreaHighEmploymentName = ""
softwareAreaHighEmployment = 0
softEmployment = 0


#For loop that opens the csv file
with open(t) as infile:
    for line in infile:
        #Splits the data acccording to the comma
        dataLine = line.split(",")
        #Opens the data if the fip code is an allowed one
        if (dataLine[0] in answer):
            #Creates an arrary of the data we need
            
            dataFromSplitLine = [dataLine[1],dataLine[2],dataLine[8],dataLine[9],dataLine[10]]
            if(dataFromSplitLine[1] == '"10"' and dataFromSplitLine[0] == '"0"'):
                if (dataLine[0] in answer):
                    #Keeps track of the fibs number
                    fipsTotalNumber = fipsTotalNumber + 1
                if (int(dataFromSplitLine[4]) > int(answer[dataLine[0]][3])):
                    answer[dataLine[0]][3] = dataFromSplitLine[4]
                answer[dataLine[0]][1] += int(dataFromSplitLine[2])
                answer[dataLine[0]][2] += int(dataFromSplitLine[3])
                #Saves the amoun of wages
                totalGrossWages += int(dataFromSplitLine[4])
                if(int(answer[dataLine[0]][3]) > totalMaxAnnualWage):
                    totalMaxAnnualWage = int(answer[dataLine[0]][3])
                    totalMaxAnnualWageName = dataLine[0]

                    # Assign Max # of Establishments

                if int(answer[dataLine[0]][1]) > totalAreaMostEstablishmentsAmount:
                    totalAreaMostEstablishmentsAmount=int(answer[dataLine[0]][1])
                    totalAreaMostEstablishmentsName=dataLine[0]
                #Saves the total amount of Establishments
                totalEstablishment += int(answer[dataLine[0]][1])

            # Assign Max Employment

                if int(answer[dataLine[0]][2]) > totalAreaHighEmployment:
                    totalAreaHighEmploymentName=dataLine[0]
                    totalAreaHighEmployment=int(answer[dataLine[0]][2])
                totalEmployment += int(answer[dataLine[0]][2])

            if(dataFromSplitLine[1] == '"5112"' and dataFromSplitLine[0] == '"5"'):
                copyArray[dataLine[0]][4] += 1
                if (dataLine[0] in copyArray):
                    #Keeps track of the software fips number
                    fipsSoftNumber = fipsSoftNumber + 1
                #Keeps the max software wages
                softGrossWages += int(dataFromSplitLine[4])
                # Maximize Annual Wage
                if int(dataFromSplitLine[4]) > int(copyArray[dataLine[0]][3]):
                    copyArray[dataLine[0]][3]=dataFromSplitLine[4]
                copyArray[dataLine[0]][2] += int(dataFromSplitLine[3])
                copyArray[dataLine[0]][1] += int(dataFromSplitLine[2])
                softEmployment+= int(copyArray[dataLine[0]][2])
                # Assign Max Wage
                if int(copyArray[dataLine[0]][3]) > softwareMaxAnnualWage:
                    softwareMaxAnnualWage=int(copyArray[dataLine[0]][3])
                    softwareMaxAnnualWageName=dataLine[0]

                # Assign Max # of Establishments
                softEstablishment += int(copyArray[dataLine[0]][1])
                if int(copyArray[dataLine[0]][1]) > softwareAreaMostEstablishmentsAmount:
                    softwareAreaMostEstablishmentsAmount=int(copyArray[dataLine[0]][1])
                    softwareAreaMostEstablishmentsName=dataLine[0]

            # Assign Max Employment

                if int(copyArray[dataLine[0]][2]) > softwareAreaHighEmployment:
                    softwareAreaHighEmploymentName=dataLine[0]
                    softwareAreaHighEmployment=int(copyArray[dataLine[0]][2])
                

            
            
            
# Create the report object
##########################
rpt=Report()


# Fill in the report for all industries
#######################################
rpt.all.num_areas= fipsTotalNumber

rpt.all.gross_annual_wages= totalGrossWages
rpt.all.max_annual_wage= (answer[totalMaxAnnualWageName][0].strip().strip('"'), totalMaxAnnualWage)

rpt.all.total_estab         = totalEstablishment
rpt.all.max_estab           = (answer[totalAreaMostEstablishmentsName][0].strip().strip('"'), totalAreaMostEstablishmentsAmount)

rpt.all.total_empl          = totalEmployment
rpt.all.max_empl            = (answer[totalAreaHighEmploymentName][0].strip().strip('"'), totalAreaHighEmployment)



# Fill in the report for the software publishing industry
#########################################################
rpt.soft.num_areas          = fipsSoftNumber

rpt.soft.gross_annual_wages = softGrossWages
rpt.soft.max_annual_wage    = (copyArray[softwareMaxAnnualWageName][0].strip().strip('"'), softwareMaxAnnualWage)

rpt.soft.total_estab        = softEstablishment
rpt.soft.max_estab          = (copyArray[softwareAreaMostEstablishmentsName][0].strip().strip('"'), softwareAreaMostEstablishmentsAmount)

rpt.soft.total_empl         = softEmployment
rpt.soft.max_empl           = (copyArray[softwareAreaHighEmploymentName][0].strip().strip('"'), softwareAreaHighEmployment)


# Print the completed report
############################
print(rpt)
